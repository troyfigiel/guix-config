(use-modules
 (gnu home)
 (gnu packages)
 (gnu services)
 (gnu home services)
 (guix gexp)
 (gnu home services shells))

(home-environment
 (packages
  (specifications->packages
   '("emacs"
     "emacs-geiser"
     "emacs-geiser-guile"
     "emacs-guix"
     "emacs-magit"
     "emacs-vertico"
     "icecat"
     "guile")))
 
 (services
  (list
   (service home-bash-service-type
            (home-bash-configuration
             (aliases '(("grep" . "grep --color=auto") ("ll" . "ls -l")
			("ls" . "ls -p --color=auto")))
             (bashrc (list (local-file "dotfiles/.bashrc"
				       "bashrc")))
             (bash-profile (list (local-file
				  "dotfiles/.bash_profile"
				  "bash_profile")))))
   (simple-service 'home-variables
		   home-environment-variables-service-type
		   '(("EDITOR" . "emacsclient -ca emacs"))))))
