(use-modules
 (gnu)
 (nongnu packages linux)
 (nongnu system linux-initrd))

(use-service-modules cups desktop networking ssh xorg)

(operating-system
 (kernel linux)
 (initrd microcode-initrd)
 (firmware (list linux-firmware))
 (locale "en_US.utf8")
 (timezone "Europe/Berlin")
 (keyboard-layout (keyboard-layout "de"))
 (host-name "vm")

 (users (cons* (user-account
                (name "troy")
                (comment "Troy Figiel")
                (group "users")
                (home-directory "/home/troy")
                (supplementary-groups
		 '("wheel" "netdev" "audio" "video")))
               %base-user-accounts))

 (packages (append (specifications->packages
			'("nss-certs"
			  "git"
			  "openssh"))
                   %base-packages))

 (services (append (list
		    (service xfce-desktop-service-type)
		    (service cups-service-type)
		    (set-xorg-configuration
		     (xorg-configuration (keyboard-layout keyboard-layout))))
		   %desktop-services))

 (bootloader (bootloader-configuration
              (bootloader grub-efi-bootloader)
              (targets (list "/boot/efi"))
              (keyboard-layout keyboard-layout)))

 (mapped-devices (list (mapped-device
			(source
			 (uuid "eb17e19c-c039-48f0-9e65-19eb962a3382"))
			(target "cryptroot")
			(type luks-device-mapping))))

 (file-systems (cons* (file-system
                       (mount-point "/boot/efi")
                       (device
			(uuid "9BFA-EF6C" 'fat32))
                       (type "vfat"))
                      (file-system
                       (mount-point "/")
                       (device "/dev/mapper/cryptroot")
                       (type "ext4")
                       (dependencies mapped-devices))
		      %base-file-systems)))
